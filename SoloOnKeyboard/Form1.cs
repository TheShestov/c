﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoloOnKeyboard
{
    public partial class Form1 : Form
    {
        //Переменные
        int total_symbols;
        int typed_symbols;
        DateTime start_time;
        DateTime current_time;
        int speed;
        int errors;        
        OpenFileDialog fd;
        OpenFileDialog fo;
        Process pResult;

        #region Результаты

        DirectoryInfo di = new DirectoryInfo("Result"); // В эту папку будем писать результаты      
  
        #endregion
        public Form1()
        {
            InitializeComponent();
            this.text_type.KeyUp += new KeyEventHandler(ErrorsCount);
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            Init();
        }
        public void выбратьФайлToolStripMenuItem_Click(object sender, EventArgs e)
        {
            fd = new OpenFileDialog();
            fd.InitialDirectory = @"C:\";
            if (fd.ShowDialog() == DialogResult.OK)
            {
                label_text.Text = File.ReadAllText(fd.FileName, Encoding.Default);
                Init();
                /*
                p = new Process();
                p.StartInfo.FileName = fd.FileName;
                StreamReader sr = new StreamReader(p);
                string allSymb = sr.ReadToEnd();
                label_text.Text = allSymb;*/
            }

        }
        private void Init()
        {
            if (label_text.Text != "")
            {
                
            }
            else
            {
                try
                {
                    label_text.Text = File.ReadAllText("solo.txt", Encoding.Default);
                }
                catch (Exception)
                {
                    label_text.Text = "Возникла ошибка! У тебя несколько вариантов: \n1. Файл не найден! Текст для набора можно разместить в файле solo.txt в той-же папке.\n2. Файл который ты выбрал - пустой и не содержит данных, для теста.";
                }

            }
            
            text_type.Text = "";
            total_symbols = label_text.Text.Length;
            typed_symbols = 0;
            start_time = DateTime.Now;
            current_time = DateTime.Now;
            speed = 0;
            errors = 0;
            timer.Enabled = true;
        }

        private void text_type_KeyUp(object sender, KeyEventArgs e)
        {
            ShowStat();
        }


        private void timer_Tick(object sender, EventArgs e)
        {
            ShowStat();
        }

        private  void ShowStat()
        {
            typed_symbols = text_type.Text.Length;
            label_type_info.Text = typed_symbols.ToString() + " / " + total_symbols.ToString() + " сим.";

            bool error;
            if (typed_symbols > 0)
            {
                if (label_text.Text.StartsWith(text_type.Text))
                {
                    error = false;
                }
                else
                    error = true;                                
            }
            else
            {
                error = false;
                start_time = DateTime.Now;
            }
            text_type.BackColor = error ? Color.LightPink : Color.White;
            
            label_errors_info.Text = errors.ToString();

            current_time = DateTime.Now;
            double seconds = (current_time.Ticks - start_time.Ticks) / 10000000.0;
            label_time_info.Text = seconds.ToString("0.0") + " сек.";
            if (seconds > 0.1)
            {
                speed = Convert.ToInt32(typed_symbols * 60 / seconds);
            }
            else
                speed = 0;
            label_speed_info.Text = speed.ToString() + " сим/мин";

            if (!error && typed_symbols == total_symbols)
                FinishGame();            
        }
        
        private void FinishGame()
        {            
            timer.Enabled = false;
            
            if (di.Exists)
            {

            }
            else
            {
                di.Create();
            }
            
            string fileName = fileNameStamp(Name);
            StreamWriter sw = File.AppendText("Result\\"+fileName);
            sw.WriteLine(label_speed_info.Text + " Ошибок: " + errors.ToString());
            sw.Close();

            MessageBox.Show("Набор закончен. Ваша скорость: " + label_speed_info.Text, "Поздравляем!");
            Init();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            about formAbout = new about();
            formAbout.Show();
        }
       
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:den@shestov.info");
        }

        //Здесь ведется подсчет ошибок
        private void ErrorsCount(Object o, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Back)
            {
                if (label_text.Text.StartsWith(text_type.Text))
                {
                    
                }
                else
                {
                    errors += 1;
                    return;
                }
            }
        }
        
        // Генерация имени файла с временным штампом
        public string fileNameStamp(string fileName)
        {
            return String.Format("Таблица-Результатов-за-{0}.txt", DateTime.Now.ToString("yyyy-MM-dd"));
        }

        private void таблицаРезультатовToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("Result");
        }
        
    }
}
