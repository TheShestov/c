﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SoloOnKeyboard
{
    public partial class about : Form
    {
        public about()
        {
            InitializeComponent();
        }

        private void about_Load(object sender, EventArgs e)
        {
            Graphics g = Graphics.FromHwnd(Handle);
            g.DrawImage(Image.FromFile("keyboard.ICO"), new Point(5, 5));
        }
    }
}
