﻿namespace SoloOnKeyboard
{
    partial class Form1
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label_text = new System.Windows.Forms.Label();
            this.text_type = new System.Windows.Forms.TextBox();
            this.label_typed = new System.Windows.Forms.Label();
            this.label_time = new System.Windows.Forms.Label();
            this.label_speed = new System.Windows.Forms.Label();
            this.label_speed_info = new System.Windows.Forms.Label();
            this.label_time_info = new System.Windows.Forms.Label();
            this.label_type_info = new System.Windows.Forms.Label();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.менюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.выбратьФайлToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.таблицаРезультатовToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.справкаToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.оПрограммеToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label_errors = new System.Windows.Forms.Label();
            this.label_errors_info = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label_text
            // 
            this.label_text.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label_text.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_text.Location = new System.Drawing.Point(2, 24);
            this.label_text.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_text.Name = "label_text";
            this.label_text.Size = new System.Drawing.Size(631, 138);
            this.label_text.TabIndex = 0;
            this.label_text.Text = "Тут будет текст";
            // 
            // text_type
            // 
            this.text_type.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.text_type.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.text_type.Location = new System.Drawing.Point(2, 164);
            this.text_type.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.text_type.Multiline = true;
            this.text_type.Name = "text_type";
            this.text_type.Size = new System.Drawing.Size(632, 147);
            this.text_type.TabIndex = 1;
            this.text_type.KeyUp += new System.Windows.Forms.KeyEventHandler(this.text_type_KeyUp);
            // 
            // label_typed
            // 
            this.label_typed.AutoSize = true;
            this.label_typed.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_typed.Location = new System.Drawing.Point(12, 319);
            this.label_typed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_typed.Name = "label_typed";
            this.label_typed.Size = new System.Drawing.Size(70, 19);
            this.label_typed.TabIndex = 2;
            this.label_typed.Text = "Набрано:";
            // 
            // label_time
            // 
            this.label_time.AutoSize = true;
            this.label_time.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_time.Location = new System.Drawing.Point(264, 319);
            this.label_time.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_time.Name = "label_time";
            this.label_time.Size = new System.Drawing.Size(54, 19);
            this.label_time.TabIndex = 3;
            this.label_time.Text = "Время:";
            // 
            // label_speed
            // 
            this.label_speed.AutoSize = true;
            this.label_speed.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_speed.Location = new System.Drawing.Point(451, 319);
            this.label_speed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_speed.Name = "label_speed";
            this.label_speed.Size = new System.Drawing.Size(74, 19);
            this.label_speed.TabIndex = 4;
            this.label_speed.Text = "Скорость:";
            // 
            // label_speed_info
            // 
            this.label_speed_info.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_speed_info.ForeColor = System.Drawing.Color.OrangeRed;
            this.label_speed_info.Location = new System.Drawing.Point(521, 319);
            this.label_speed_info.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_speed_info.Name = "label_speed_info";
            this.label_speed_info.Size = new System.Drawing.Size(106, 19);
            this.label_speed_info.TabIndex = 7;
            this.label_speed_info.Text = "-";
            this.label_speed_info.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label_time_info
            // 
            this.label_time_info.AutoSize = true;
            this.label_time_info.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_time_info.ForeColor = System.Drawing.Color.OrangeRed;
            this.label_time_info.Location = new System.Drawing.Point(335, 319);
            this.label_time_info.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_time_info.Name = "label_time_info";
            this.label_time_info.Size = new System.Drawing.Size(15, 19);
            this.label_time_info.TabIndex = 6;
            this.label_time_info.Text = "-";
            // 
            // label_type_info
            // 
            this.label_type_info.AutoSize = true;
            this.label_type_info.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_type_info.ForeColor = System.Drawing.Color.OrangeRed;
            this.label_type_info.Location = new System.Drawing.Point(96, 319);
            this.label_type_info.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label_type_info.Name = "label_type_info";
            this.label_type_info.Size = new System.Drawing.Size(15, 19);
            this.label_type_info.TabIndex = 5;
            this.label_type_info.Text = "-";
            // 
            // timer
            // 
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.менюToolStripMenuItem,
            this.справкаToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(635, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // менюToolStripMenuItem
            // 
            this.менюToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.выбратьФайлToolStripMenuItem,
            this.таблицаРезультатовToolStripMenuItem});
            this.менюToolStripMenuItem.Name = "менюToolStripMenuItem";
            this.менюToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.менюToolStripMenuItem.Text = "Меню";
            // 
            // выбратьФайлToolStripMenuItem
            // 
            this.выбратьФайлToolStripMenuItem.Name = "выбратьФайлToolStripMenuItem";
            this.выбратьФайлToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.выбратьФайлToolStripMenuItem.Text = "...выбрать файл";
            this.выбратьФайлToolStripMenuItem.Click += new System.EventHandler(this.выбратьФайлToolStripMenuItem_Click);
            // 
            // таблицаРезультатовToolStripMenuItem
            // 
            this.таблицаРезультатовToolStripMenuItem.Name = "таблицаРезультатовToolStripMenuItem";
            this.таблицаРезультатовToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.таблицаРезультатовToolStripMenuItem.Text = "Папка с результатами";
            this.таблицаРезультатовToolStripMenuItem.Click += new System.EventHandler(this.таблицаРезультатовToolStripMenuItem_Click);
            // 
            // справкаToolStripMenuItem
            // 
            this.справкаToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.справкаToolStripMenuItem1,
            this.оПрограммеToolStripMenuItem});
            this.справкаToolStripMenuItem.Name = "справкаToolStripMenuItem";
            this.справкаToolStripMenuItem.Size = new System.Drawing.Size(26, 20);
            this.справкаToolStripMenuItem.Text = "?";
            // 
            // справкаToolStripMenuItem1
            // 
            this.справкаToolStripMenuItem1.Name = "справкаToolStripMenuItem1";
            this.справкаToolStripMenuItem1.Size = new System.Drawing.Size(152, 22);
            this.справкаToolStripMenuItem1.Text = "Справка";
            this.справкаToolStripMenuItem1.Visible = false;
            // 
            // оПрограммеToolStripMenuItem
            // 
            this.оПрограммеToolStripMenuItem.Name = "оПрограммеToolStripMenuItem";
            this.оПрограммеToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.оПрограммеToolStripMenuItem.Text = "О программе";
            this.оПрограммеToolStripMenuItem.Click += new System.EventHandler(this.оПрограммеToolStripMenuItem_Click);
            // 
            // label_errors
            // 
            this.label_errors.AutoSize = true;
            this.label_errors.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_errors.Location = new System.Drawing.Point(12, 357);
            this.label_errors.Name = "label_errors";
            this.label_errors.Size = new System.Drawing.Size(67, 19);
            this.label_errors.TabIndex = 9;
            this.label_errors.Text = "Ошибок:";
            // 
            // label_errors_info
            // 
            this.label_errors_info.AutoSize = true;
            this.label_errors_info.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_errors_info.ForeColor = System.Drawing.Color.OrangeRed;
            this.label_errors_info.Location = new System.Drawing.Point(96, 357);
            this.label_errors_info.Name = "label_errors_info";
            this.label_errors_info.Size = new System.Drawing.Size(15, 19);
            this.label_errors_info.TabIndex = 10;
            this.label_errors_info.Text = "-";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(635, 383);
            this.Controls.Add(this.label_errors_info);
            this.Controls.Add(this.label_errors);
            this.Controls.Add(this.label_speed_info);
            this.Controls.Add(this.label_time_info);
            this.Controls.Add(this.label_type_info);
            this.Controls.Add(this.label_speed);
            this.Controls.Add(this.label_time);
            this.Controls.Add(this.label_typed);
            this.Controls.Add(this.text_type);
            this.Controls.Add(this.label_text);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 4, 2, 4);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Проверка скорости печати";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_text;
        private System.Windows.Forms.TextBox text_type;
        private System.Windows.Forms.Label label_typed;
        private System.Windows.Forms.Label label_time;
        private System.Windows.Forms.Label label_speed;
        private System.Windows.Forms.Label label_speed_info;
        private System.Windows.Forms.Label label_time_info;
        private System.Windows.Forms.Label label_type_info;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem менюToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem выбратьФайлToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem таблицаРезультатовToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem справкаToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem оПрограммеToolStripMenuItem;
        private System.Windows.Forms.Label label_errors;
        private System.Windows.Forms.Label label_errors_info;
    }
}

